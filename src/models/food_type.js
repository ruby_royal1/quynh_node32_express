import { DataTypes } from "sequelize";
import { sequelize } from "./db.js";

// Định nghĩa mô hình FoodType
const FoodType = sequelize.define("FoodType", {
  type_id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  type_name: {
    type: DataTypes.STRING,
    allowNull: false,
  },
});

export default FoodType;
