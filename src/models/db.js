import { Sequelize } from "sequelize";

// Tạo đối tượng Sequelize với các thông tin kết nối đến cơ sở dữ liệu
const sequelize = new Sequelize("db_food", "root", "1234", {
  host: "localhost",
  dialect: "mysql",
});

export { sequelize };
