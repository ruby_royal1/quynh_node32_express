import { DataTypes } from "sequelize";
import { sequelize } from "./db.js";
import User from "./user.js";
import Restaurant from "./restaurant.js";

const RateRes = sequelize.define("rate_res", {
  user_id: {
    type: DataTypes.INTEGER,
    allowNull: false,
  },
  res_id: {
    type: DataTypes.INTEGER,
    allowNull: false,
  },
  amount: {
    type: DataTypes.INTEGER,
    allowNull: false,
  },
  date_rate: {
    type: DataTypes.DATE,
    allowNull: false,
  },
});

RateRes.belongsTo(User, { foreignKey: "user_id" });
RateRes.belongsTo(Restaurant, { foreignKey: "res_id" });

export default RateRes;
