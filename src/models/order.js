import { DataTypes } from "sequelize";
import { sequelize } from "./db.js";

// Định nghĩa mô hình Order
const Order = sequelize.define("Order", {
  user_id: {
    type: DataTypes.INTEGER,
    allowNull: false,
  },
  food_id: {
    type: DataTypes.INTEGER,
    allowNull: false,
  },
  amount: {
    type: DataTypes.INTEGER,
    allowNull: false,
  },
  code: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  arr_sub_id: {
    type: DataTypes.STRING,
    allowNull: false,
  },
});

export default Order;
