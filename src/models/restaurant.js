import { DataTypes } from "sequelize";
import { sequelize } from "./db.js";

const Restaurant = sequelize.define("restaurant", {
  res_id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  res_name: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  image: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  description: {
    type: DataTypes.STRING,
    allowNull: false,
  },
});

export default Restaurant;
