import { DataTypes } from "sequelize";
import { sequelize } from "./db.js";

// Định nghĩa mô hình SubFood
const SubFood = sequelize.define("SubFood", {
  sub_id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  sub_name: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  sub_price: {
    type: DataTypes.FLOAT,
    allowNull: false,
  },
  food_id: {
    type: DataTypes.INTEGER,
    allowNull: false,
  },
});

export default SubFood;
