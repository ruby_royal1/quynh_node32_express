import { DataTypes } from "sequelize";
import { sequelize } from "./db.js";
import User from "./user.js";
import Restaurant from "./restaurant.js";

const LikeRes = sequelize.define("like_res", {
  like_id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  user_id: {
    type: DataTypes.INTEGER,
    allowNull: false,
  },
  date_like: {
    type: DataTypes.DATE,
    allowNull: true,
  },
});

LikeRes.belongsTo(User, { foreignKey: "user_id" });
LikeRes.belongsTo(Restaurant, { foreignKey: "res_id" });

export default LikeRes;
