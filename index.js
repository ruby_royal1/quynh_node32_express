import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
import { sequelize } from "./src/models/db.js";
import User from "./src/models/user.js";
import Food from "./src/models/food.js";
import FoodType from "./src/models/food_type.js";
import SubFood from "./src/models/sub_food.js";
import Order from "./src/models/order.js";
import Rating from "./src/models/rate_res.js";
import Like from "./src/models/like_res.js";
import Restaurant from "./src/models/restaurant.js";
// const { sequelize } = require("./models/db.js");
// const User = require("./models/user.js");
// const Food = require("./models/food.js");
// const FoodType = require("./models/food_type.js");
// const SubFood = require("./models/sub_food.js");
// const Order = require("./models/order.js");
// const Rating = require("./models/rating.js");
// const Like = require("./models/like.js");

const app = express();
app.listen(8080);
app.use(bodyParser.json());
app.use(cors());

// Tạo các liên kết giữa các bảng
Food.belongsTo(FoodType, { foreignKey: "type_id" });
SubFood.belongsTo(Food, { foreignKey: "food_id" });
Order.belongsTo(User, { foreignKey: "user_id" });
Order.belongsTo(Food, { foreignKey: "food_id" });
Rating.belongsTo(User, { foreignKey: "user_id" });
Rating.belongsTo(Restaurant, { foreignKey: "res_id" });
Like.belongsTo(User, { foreignKey: "user_id" });
Like.belongsTo(Restaurant, { foreignKey: "res_id" });

// Định nghĩa các API endpoints
app.post("/api/user/:user_id/like", (req, res) => {
  const { user_id } = req.params;
  const { res_id } = req.body;

  Like.create({ user_id, res_id })
    .then(() => {
      res
        .status(200)
        .json({ success: true, message: "Liked restaurant successfully" });
    })
    .catch((error) => {
      res
        .status(500)
        .json({ success: false, message: "Failed to like restaurant", error });
    });
});

app.delete("/api/user/:user_id/like/:res_id", (req, res) => {
  const { user_id, res_id } = req.params;

  Like.destroy({ where: { user_id, res_id } })
    .then(() => {
      res
        .status(200)
        .json({ success: true, message: "Unliked restaurant successfully" });
    })
    .catch((error) => {
      res.status(500).json({
        success: false,
        message: "Failed to unlike restaurant",
        error,
      });
    });
});

app.get("/api/user/:user_id/likes", (req, res) => {
  const { user_id } = req.params;

  Like.findAll({
    where: { user_id },
    attributes: ["user_id", "res_id", "date_like"],
  })
    .then((likes) => {
      res.status(200).json({ success: true, likes });
    })
    .catch((error) => {
      res
        .status(500)
        .json({ success: false, message: "Failed to get user likes", error });
    });
});

app.post("/api/user/:user_id/rating", (req, res) => {
  const { user_id } = req.params;
  const { res_id, rating } = req.body;

  Rating.create({ user_id, res_id, rating })
    .then(() => {
      res
        .status(200)
        .json({ success: true, message: "Rating added successfully" });
    })
    .catch((error) => {
      res
        .status(500)
        .json({ success: false, message: "Failed to add rating", error });
    });
});

app.get("/api/user/:user_id/rating", (req, res) => {
  const { user_id } = req.params;

  Rating.findAll({
    where: { user_id },
    attributes: ["user_id", "res_id", "amount", "date_rate"],
  })
    .then((Rating) => {
      res.status(200).json({ success: true, Rating });
    })
    .catch((error) => {
      res
        .status(500)
        .json({ success: false, message: "Failed to get user ratings", error });
    });
});

app.post("/api/user/:user_id/order", (req, res) => {
  const { user_id } = req.params;
  const { food_id, amount, code, arr_sub_id } = req.body;

  Order.create({ user_id, food_id, amount, code, arr_sub_id })
    .then(() => {
      res
        .status(200)
        .json({ success: true, message: "Order placed successfully" });
    })
    .catch((error) => {
      res
        .status(500)
        .json({ success: false, message: "Failed to place order", error });
    });
});

// Khởi tạo cơ sở dữ liệu và bắt đầu lắng nghe các kết nối
// sequelize
//   .sync()
//   .then(() => {
//     console.log("Database synced");
//     app.listen(8080, () => {
//       console.log("Server started on port 8080");
//     });
//   })
//   .catch((error) => {
//     console.log("Failed to sync database", error);
//   });
